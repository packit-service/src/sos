%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global auditversion 0.3

Summary: A set of tools to gather troubleshooting information from a system
Name: sos
Version: 3.9.1
Release: 6%{?dist}
Group: Applications/System
Source0: https://github.com/sosreport/sos/archive/%{version}/sos-%{version}.tar.gz
Source1: sos-audit-%{auditversion}.tgz
License: GPLv2+
BuildArch: noarch
Url: http://github.com/sosreport/sos
BuildRequires: python3-devel
BuildRequires: python3-six
BuildRequires: gettext
Requires: libxml2-python3
Requires: bzip2
Requires: xz
Requires: python3-six
Conflicts: vdsm < 4.40
Patch1: sos-bz1825283-env-host-in-container.patch
Patch2: sos-bz1823488-containers-common.patch
Patch3: sos-bz1819662-rabbitmqctl-on-foreground.patch
Patch4: sos-bz1814867-insights-archive.patch
Patch5: sos-bz1785546-nvmetcli.patch
Patch6: sos-bz1776549-podman-buildah-rootless.patch
Patch7: sos-bz1633006-iptables-kmods.patch
Patch8: sos-bz1457191-navicli-noniteractively.patch
Patch9: sos-bz1838123-xdp-plugin.patch
Patch10: sos-bz1843562-gluster-volume-specific.patch
Patch11: sos-bz1844853-nfs-etc-exports.patch
Patch12: sos-bz1845386-pacemaker-passwords-with-equal-sign.patch
Patch13: sos-bz1843754-powerpc-logs-for-components.patch
Patch14: sos-bz1850926-logs-no-journal.patch
Patch15: sos-bz1850554-luks-not-detected.patch
Patch16: sos-bz1851923-powerpc-missing-logs.patch
Patch17: sos-bz1853700-pci-too-strong-condition.patch
Patch18: sos-bz1857590-gluster-removes-sockfiles.patch
Patch19: sos-bz1859888-kubernetes-indexerror-on-nodes.patch
Patch20: sos-bz1869724-ethtool-not-on-bnx2x.patch

%description
Sos is a set of tools that gathers information about system
hardware and configuration. The information can then be used for
diagnostic purposes and debugging. Sos is commonly used to help
support technicians and developers.

%prep
%setup -qn %{name}-%{version}
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1
%patch14 -p1
%patch15 -p1
%patch16 -p1
%patch17 -p1
%patch18 -p1
%patch19 -p1
%patch20 -p1
%setup -T -D -a1 -q

%build
%py3_build

%install
%py3_install '--install-scripts=%{_sbindir}'

install -Dm644 %{name}.conf %{buildroot}%{_sysconfdir}/%{name}.conf

%find_lang %{name} || echo 0

cd %{name}-audit-%{auditversion}
DESTDIR=%{buildroot} ./install.sh
cd ..

%files -f %{name}.lang
%{_sbindir}/sosreport
%{python3_sitelib}/*
%{_mandir}/man1/sosreport.1.gz
%{_mandir}/man5/sos.conf.5.gz
%doc AUTHORS README.md
%license LICENSE
%config(noreplace) %{_sysconfdir}/sos.conf

%package audit
Summary: Audit use of some commands for support purposes
License: GPLv2+
Group: Application/System

%description audit

Sos-audit provides configuration files for the Linux Auditing System
to track the use of some commands capable of changing the configuration
of the system.  Currently storage and filesystem commands are audited.

%post audit
%{_sbindir}/sos-audit.sh

%files audit
%defattr(755,root,root,-)
%{_sbindir}/sos-audit.sh
%defattr(644,root,root,-)
%config(noreplace) %{_sysconfdir}/sos/sos-audit.conf
%defattr(444,root,root,-)
%{_prefix}/lib/sos/audit/*
%{_mandir}/man5/sos-audit.conf.5.gz
%{_mandir}/man8/sos-audit.sh.8.gz
%ghost /etc/audit/rules.d/40-sos-filesystem.rules
%ghost /etc/audit/rules.d/40-sos-storage.rules

%changelog
* Wed Aug 19 2020 Pavel Moravec <pmoravec@redhat.com> = 3.9.1-6
- [networking] remove 'ethtool -e' option for bnx2x NICs
  Resolves: bz1869724

* Fri Jul 24 2020 Pavel Moravec <pmoravec@redhat.com> = 3.9.1-5
- [logs] collect also non-persistent journal logs
  Resolves: bz1850926
- [block] Fix typo in LUKS detection
  Resolves: bz1850554
- [powerpc] Fix enablement triggers
  Resolves: bz1851923
- [pci] Update gating for lspci commands
  Resolves: bz1853700
- [containers_common] collect user-related commands outputs
  Resolves: bz1776549
- [gluster] remove only dump files + generated state files
  Resolves: bz1857590
- [kubernetes] ignore blank+empty lines in "kubectl get nodes"
  Resolves: bz1859888

* Tue Jun 23 2020 Pavel Moravec <pmoravec@redhat.com> = 3.9.1-4
- [gluster] fix gluster volume splitlines iteration
  Resolves: bz1843562
- [nfs] merge nfsserver plugin into nfs one
  Resolves: bz1844853
- [pacemaker] Fix scrubbing when password contains an equa
  Resolves: bz1845386
- [powerpc] Add support to collect component logs
  Resolves: bz1843754

* Wed May 27 2020 Pavel Moravec <pmoravec@redhat.com> = 3.9.1-2
- Rebase on upstream 3.9
  Resolves: bz1826656
- [redhat] fix RH containers without sysroot Attempting to run
  Resolves: bz1825283
- [containers_common] Add plugin for common containers configs
  Resolves: bz1823488
- [rabbitmq] Call containerised rabbitmqctl report on
  Resolves: bz1819662
- [insights] collect insights-client dump
  Resolves: bz1814867
- [nvmetcli] Add new plugin for NVMe Target CLI
  Resolves: bz1785546
- [containers_common] collect rootless containers info
  Resolves: bz1776549
- [networking] collect iptables when proper kernel modules
  Resolves: bz1633006
- [navicli] replace interactive prompt by plugin option
  Resolves: bz1457191
- [xdp] Add XDP plugin
  Resolves: bz1838123

* Thu May 21 2020 Pavel Moravec <pmoravec@redhat.com> = 3.8-4
- [container_log] fix unscoped 'logdir' variable
  Resolves: bz1834421

* Wed May 06 2020 Pavel Moravec <pmoravec@redhat.com> = 3.8-3
- [containers_common] Add plugin for common containers configs
  Resolves: bz1823488

* Fri Jan 10 2020 Pavel Moravec <pmoravec@redhat.com> = 3.8-2
- [plugins] improve heuristic for applying --since
  Resolves: bz1789049
- [Predicate] Override __bool__ to allow py3 evaluation
  Resolves: bz1789018
- [ceph] Add 'ceph insights' command output
  Resolves: bz1783034
- [dnf] Collect dnf module list
  Resolves: bz1781819
- [kernel,networking] collect bpftool net list for each
  Resolves: bz1768956
- [libreswan] New plugin for "libreswan" IPsec
  Resolves: bz1741330
- [kernel] collect "bpftool net list"
  Resolves: bz1721779
- [grub2] call grub2-config with --no-grubenv-update
  Resolves: bz1709682

* Wed Dec 11 2019 Pavel Moravec <pmoravec@redhat.com> = 3.8-1
- Rebase on upstream 3.8
  Resolves: bz1779387

* Mon Nov 04 2019 Pavel Moravec <pmoravec@redhat.com> = 3.7-7
- [Plugin, kernel] interim sysroot fixes
  Resolves: bz1766915

* Wed Oct 30 2019 Pavel Moravec <pmoravec@redhat.com> = 3.7-6
- [ovirt_hosted_engine] Add gluster deployment and cleanup log
  Resolves: bz1744086
- [vdsm]: Fix executing shell commands
  Resolves: bz1744110
- [ovn_*] Add support to containerized setups
  Resolves: bz1744553
- [ipa] collect ipa-healthcheck logs, kdcproxy configs, httpd cert
  Resolves: bz1688764

* Wed Oct 02 2019 Pavel Moravec <pmoravec@redhat.com> = 3.7-5
- [kernel] Don't collect trace file by default
  Resolves: bz1738391

* Thu Sep 12 2019 Pavel Moravec <pmoravec@redhat.com> = 3.7-4
- [openvswitch] catch all openvswitch2.* packages
  Resolves: bz1745017

* Tue Jul 30 2019 Pavel Moravec <pmoravec@redhat.com> = 3.7-3
- [openstack] Extract Placement plugin from Nova
  Resolves: bz1717882
- [utilities] Fix high CPU usage and slow command collection
  Resolves: bz1733352
- [peripety] collect proper config file
  Resolves: bz1665981
- [sosreport,plugins] Stop plugin execution after timeout hit
  Resolves: bz1733469
- [nvme] collect config file everytime
  Resolves: bz1665929

* Tue Jul 09 2019 Pavel Moravec <pmoravec@redhat.com> = 3.7-2
- [sar] collect whole sar log dir
  Resolves: bz1714243
- [archive] convert absolute symlink targets to relative
  Resolves: bz1702806
- [archive] Handle checking container sysroot in _make_leading_paths
  Resolves: bz1728214
- [frr] FRR plugin
  Resolves: bz1709906
- [policies] redhat policy to use hostname instead of rhn id
  Resolves: bz1718087
- Updates to vdsm plugin
  Resolves: bz1700780

* Wed Jun 12 2019 Pavel Moravec <pmoravec@redhat.com> = 3.7-1
- Rebase on upstream 3.7
  Resolves: bz1684400
- [buildah] parse container list properly even for scratch ones
  Resolves: bz1687954
- [PATCH] [maas,mysql,npm,pacemaker,postgresql] fix plugopts data types
  Resolves: bz1695583
- [plugins] add vdsm plugin
  Resolves: bz1700780
- [openstack_instack] add ansible.log
  Resolves: bz1702806
- [pcp] collect pmlogger without a sizelimit
  Resolves: bz1719884
- [foreman,satellite] increase plugin default timeouts
  Resolves: bz1719885
- [sosreport] [sosreport] initialize disabled plugins properly
  Resolves: bz1719886
- [katello] support both locations of qpid SSL certs
  Resolves: bz1719887

* Thu May 02 2019 Pavel Moravec <pmoravec@redhat.com> = 3.6-11
- [composer] Collect sources info for all sources
  Resolves: bz1678418

* Mon Jan 21 2019 Pavel Moravec <pmoravec@redhat.com> = 3.6-10
- [grub2] Enable plugin by grub2-common package also
  Resolves: bz1666214

* Mon Jan 14 2019 Pavel Moravec <pmoravec@redhat.com> = 3.6-9
- [block] proper parsing of luks partition on self device
  Resolves: bz1638855
- [networking] Collect NUMA Node of each NIC
  Resolves: bz1645085
- [composer] add missing commas in list in add_copy_spec
  Resolves: bz1644062
- [opendaylight] Update directory for openDaylight logs
  Resolves: bz1642377

* Fri Dec 13 2018 Pavel Moravec <pmoravec@redhat.com> = 3.6-8
- [plugins] fix exception when collecting empty strings
  Resolves: bz1632607
- [crypto] collect more configs and commands
  Resolves: bz1638492
- [networking] Replace "brctl: by "bridge" commands
  Resolves: bz1644021
- [firewalld] collect nftables ruleset
  Resolves: bz1644022
- [composer] New plugin for lorax-composer
  Resolves: bz1644062
- [Plugin] clean up Plugin.get_option()
  Resolves: bz1655984
- [ovirt_node] New plugin for oVirt Node
  Resolves: bz1658937
- [podman] Add support for gathering information on podman
  Resolves: bz1658938
- [postgresql] Do not limit dump size
  Resolves: bz1658939

* Fri Oct 12 2018 Pavel Moravec <pmoravec@redhat.com> = 3.6-7
- [plugin,archive] fix remaining add_link issues
  Resolves: bz1627543
- [kernel] dont collect some tracing instance files
  Resolves: bz1638637
- [openstack_*] relax enabling of OSP RedHat plugins
  Resolves: bz1638638
- [powerpc] Add support to collect DLPAR and LPM related logs
  Resolves: bz1637127

* Mon Sep 10 2018 Pavel Moravec <pmoravec@redhat.com> = 3.6-6
- [archive] fix leading path creation
  Resolves: bz1627543
- [atomic] Define valid preset for RHEL Atomic
  Resolves: bz1627546
- [utilities] wait till AsyncReader p.poll() returns None
  Resolves: bz1627544

* Thu Aug 23 2018 Pavel Moravec <pmoravec@redhat.com> = 3.6-5
- [rhv-log-collector-analyzer] Add new plugin for RHV
  Resolves: bz1620049
- [kubernetes|etcd] Support OpenShift 3.10 deployments
  Resolves: bz1620048
- [krb5|gssproxy] add new plugin, collect more krb5 files
  Resolves: bz1607630
- [block] collect luksDump for all encrypted devices
  Resolves: bz1599739
- [archive] Dont copystat /sys and /proc paths
  Resolves: bz1619234

* Fri Aug 10 2018 Pavel Moravec <pmoravec@redhat.com> = 3.6-4
- [apparmor,ceph] fix typo in add_forbidden_path
  Resolves: bz1614955
- [policies] sanitize report label
  Resolves: bz1614956
- [policies,process] make lsof execution optional, dont call on RHOSP
  Resolves: bz1614957
- [sosreport] Add mechanism to encrypt final archive
  Resolves: bz1614952
- [archive] fix stat typo
  Resolves: bz1614953
- [rhui] Fix detection of CDS for RHUI3
  Resolves: bz1614954
- [archive] fix add_string()/do_*_sub() regression
  Resolves: bz1599701

* Fri Aug 10 2018 Bryn M. Reeves <bmr@redhat.com> = 3.6-3
- Clean up spec file and sources
- Integrate sos-audit subpackage
  Resolves: bz1601084

* Tue Jul 10 2018 Pavel Moravec <pmoravec@redhat.com> = 3.6-2
- Rebase on upstream 3.6
  Resolves: bz1549522

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Tue Nov 14 2017 Sandro Bonazzola <sbonazzo@fedoraproject.org> - 3.5-1
- Rebase on upstream 3.5
- Resolves: BZ#1513030

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Wed Mar 29 2017 Sandro Bonazzola <sbonazzo@fedoraproject.org> - 3.4-1
- Rebase on upstream 3.4
- Resolves: BZ#1436969
- Resolves: BZ#1427445

* Thu Feb 23 2017 Sandro Bonazzola <sbonazzo@fedoraproject.org> - 3.3-1
- Rebase on upstream 3.3
- Resolves: BZ#1411314

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 3.2-5
- Rebuild for Python 3.6

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2-4
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 3.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Dec 16 2015 Bryn M. Reeves <bmr@redhat.com> = 3.2-2
- [sosreport] ensure private temporary directory is removed
- [global] sync rawhide package with upstream
- [ceph] collect /var/lib/ceph and /var/run/ceph
- [sosreport] prepare report in a private subdirectory (CVE-2015-7529)
- [docker] collect journald logs for docker unit
- [sosreport] fix command-line report defaults
- [openstack_neutron] obfuscate server_auth in restproxy.ini
- [memory] collect swapon --show output in bytes
- [sosreport] fix command-line report defaults (proper patch ordering)
- [sapnw] call self methods properly
- [openvswitch] capture the logs, db and OVS bridges details
- [logs] fix reference to missing 'rsyslog_conf' variable
- [sapnw] Add check if saphostctrl is not present, dont use Set
- [Plugin] fix handling of symlinks in non-sysroot environments
- [openstack] Ensure openstack passwords and secrets are obfuscated
- [plugin] pass stderr through _collect_cmd_output
- [kubernetes,plugin] Support running sos inside a container
- [openstack] New Openstack Trove (DBaaS) plugin
- [services] Add more diagnostics to applications
- [openstack_neutron] Obscure passwords and secrets
- [ceph] add calamari and ragos logs and configs
- [iprconfig] enable plugin for ppc64* architectures
- [general] verify --profile contains valid plugins only
- [kernel,mpt,memory] additional kernel-related diagnostics
- [cluster] enable crm_report password scrubbing
- [sosreport] fix command-line report defaults
- [virsh] add new plugin, add listing of qemu
- [sap*,vhostmd] new plugins for SAP
- [cluster] crm_report fails to run because dir already exists
- [foreman] Skip collection of generic resources
- [apache] Added collection of conf.modules.d dir for httpd 2.4
- [pcp] collect /etc/pcp.conf
- [puppet] adding new plugin for puppet
- [block] Don't use parted human readable output
- [general] Better handling --name and --ticket-number in
- [networking] additional ip, firewall and traffic shaping
- [infiniband] add opensm and infiniband-diags support
- [plugins/rabbitmq] Added cluster_status command output
- [networking] re-add 'ip addr' with a root symlink
- [kimchi] add new plugin
- [iprconfig] add plugin for IBM Power RAID adapters
- [ovirt] Collect engine tunables and domain information.
- [activemq] Honour all_logs and get config on RHEL
- [cluster] Add luci to packages for standalone luci servers
- [hpasm] hpasmcli commands hang under timeout
- [mysql] Collect log file
- [chrony] add chrony plugin
- [openstack_sahara] redact secrets from sahara configuration
- [openstack_sahara] add new openstack_sahara plugin
- [openstack_neutron] neutron configuration and logs files not captured
- [ovirt] remove ovirt-engine setup answer file password leak
- [networking] network plugin fails if NetworkManager is disabled
- [cluster] crm_report fails to run because dir already exists
- [mysql] improve handling of dbuser, dbpass and MYSQL_PWD
- [mysql] test for boolean values in dbuser and dbpass
- [plugin] limit path names to PC_NAME_MAX
- [squid] collect files from /var/log/squid
- [sosreport] log plugin exceptions to a file
- [ctdb] fix collection of /etc/sysconfig/ctdb
- [sosreport] fix silent exception handling
- [sosreport] do not make logging calls after OSError
- [sosreport] catch OSError exceptions in SoSReport.execute()
- [anaconda] make useradd password regex tolerant of whitespace
- [mysql] fix handling of mysql.dbpass option
- [navicli] catch exceptions if stdin is unreadable
- [docs] update man page for new options
- [sosreport] make all utf-8 handling user errors=ignore
- [kpatch] do not attempt to collect data if kpatch is not installed
- [archive] drop support for Zip archives
- [sosreport] fix archive permissions regression
- [tomcat] add support for tomcat7 and default log size limits
- [mysql] obtain database password from the environment
- [corosync] add postprocessing for corosync-objctl output
- [ovirt_hosted_engine] fix exception when force-enabled
- [yum] call rhsm-debug with --no-subscriptions
- [powerpc] allow PowerPC plugin to run on ppc64le
- [package] add Obsoletes for sos-plugins-openstack
- [pam] add pam_tally2 and faillock support
- [postgresql] obtain db password from the environment
- [pcp] add Performance Co-Pilot plugin
- [nfsserver] collect /etc/exports.d
- [sosreport] handle --compression-type correctly
- [anaconda] redact passwords in kickstart configurations
- [haproxy] add new plugin
- [keepalived] add new plugin
- [lvm2] set locking_type=0 when calling lvm commands
- [tuned] add new plugin
- [cgroups] collect /etc/sysconfig/cgred
- [plugins] ensure doc text is always displayed for plugins
- [sosreport] fix the distribution version API call
- [docker] add new plugin
- [openstack_*] include broken-out openstack plugins
- [mysql] support MariaDB
- [openstack] do not collect /var/lib/nova
- [grub2] collect grub.cfg on UEFI systems
- [sosreport] handle out-of-space errors gracefully
- [firewalld] new plugin
- [networking] collect NetworkManager status
- [kpatch] new plugin
- [global] update to upstream 3.2 release
- [foreman] add new plugin

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2-0.4.a
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Fri Jul 17 2015 Miro Hrončok <mhroncok@redhat.com> - 3.2-0.3.a
- Use Python 3 (#1014595)
- Use setup.py instead of make
- Remove some deprecated statements

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2-0.2.a
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Tue Jun 17 2014 Bryn M. Reeves <bmr@redhat.com> = 3.2-0.1.a
- Make source URL handling compliant with packaging guidelines
- Update to new upstream pre-release sos-3.2-alpha1

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Apr 01 2014 Bryn M. Reeves <bmr@redhat.com> = 3.1-1
- Update to new upstream release sos-3.1
- Add collection of grub configuration for UEFI systems
- Raise a TypeError if add_copy_specs() is called with a string
- Add tests for Plugin.add_copy_spec()/add_copy_specs()
- Update Plugin tests to treat copy_paths as a set
- Use a set for Plugin.copy_paths
- Remove references to 'sub' parameter from plugin tests
- Remove 'sub' parameter from Plugin.add_copy_spec*()
- Drop RedHatPlugin from procenv
- Update plugin_tests.py to match new method names
- Remove obsolete checksum reference from utilities_tests.py
- Refactor Plugin.collect() pathway
- Fix x86 arch detection in processor plugin
- Pythonify Plugin._path_in_pathlist()
- Clean up package checks in processor plugin
- Replace self.policy().pkg_by_name() us in Logs plugin
- Convert infiniband to package list
- Dead code removal: PluginException
- Dead code removal: sos.plugins.common_prefix()
- Add vim tags to all python source files
- Dead code removal: utilities.checksum()
- Dead code removal: DirTree
- Dead code removal: sos_relative_path()
- Remove --profile support
- Fix plugin_test exception on six.PY2
- Call rhsm-debug with the --sos switch
- Do not collect isos in cobbler plugin
- Match plugins against policies
- Update policy_tests.py for validate_plugin change
- Rename validatePlugin to validate_plugin
- Fix broken binary detection in satellite plugin
- Clean up get_cmd_path/make_cmd_path/make_cmd_dirs mess
- Add tuned plugin
- Update systemd support
- Fix remaining use of obsolete 'get_cmd_dir()' in plugins
- Add PowerNV specific debug data
- powerpc: Move VPD related tool under common code
- Remove the rhevm plugin.
- Replace package check with file check in anacron
- Scrub ldap_default_authtok password in sssd plugin
- Eliminate hard-coded /var/log/sa paths in sar plugin
- Remove useless check_enabled() from sar plugin
- Improve error message when cluster.crm_from is invalid
- Fix command output substitution exception
- Add distupgrade plugin
- Fix gluster volume name extraction
- Ensure unused fds are closed when calling subprocesses via Popen
- Pass --no-archive to rhsm-debug script
- postgresql: allow use TCP socket
- postgresql: added license and copyright
- postgresql: add logs about errors / warnings
- postgresql: minor fixes
- Include geo-replication status in gluster plugin
- Make get_cmd_output_now() behaviour match 2.2
- Add rhsm-debug collection to yum plugin
- Always treat rhevm vdsmlogs option as string
- Fix verbose file logging
- Fix get_option() use in cluster plugin
- Fix cluster postproc regression
- Ensure superclass postproc method is called in ldap plugin
- Remove obsolete diagnostics code from ldap plugin
- Fix cluster module crm_report support

* Thu Mar 20 2014 Bryn M. Reeves <bmr@redhat.com> = 3.0-23
- Call rhsm-debug with the --sos switch

* Mon Mar 03 2014 Bryn M. Reeves <bmr@redhat.com>
- Fix package check in anacron plugin

* Wed Feb 12 2014 Bryn M. Reeves <bmr@redhat.com>
- Remove obsolete rhel_version() usage from yum plugin

* Tue Feb 11 2014 Bryn M. Reeves <bmr@redhat.com>
- Prevent unhandled exception during command output substitution

* Mon Feb 10 2014 Bryn M. Reeves <bmr@redhat.com>
- Fix generation of volume names in gluster plugin
- Add distupgrade plugin

* Tue Feb 04 2014 Bryn M. Reeves <bmr@redhat.com>
- Prevent file descriptor leaks when using Popen
- Disable zip archive creation when running rhsm-debug
- Include volume geo-replication status in gluster plugin

* Mon Feb 03 2014 Bryn M. Reeves <bmr@redhat.com>
- Fix get_option use in cluster plugin
- Fix debug logging to file when given '-v'
- Always treat rhevm plugin's vdsmlogs option as a string
- Run the rhsm-debug script from yum plugin

* Fri Jan 31 2014 Bryn M. Reeves <bmr@redhat.com>
- Add new plugin to collect OpenHPI configuration
- Fix cluster plugin crm_report support
- Fix file postprocessing in ldap plugin
- Remove collection of anaconda-ks.cfg from general plugin

* Fri Jan 24 2014 Bryn M. Reeves <bmr@redhat.com>
- Remove debug statements from logs plugin
- Make ethernet interface detection more robust
- Fix specifying multiple plugin options on the command line
- Make log and message levels match previous versions
- Log a warning message when external commands time out
- Remove --upload command line option
- Update sos UI text to match upstream

* Fri Dec 27 2013 Daniel Mach <dmach@redhat.com>
- Mass rebuild 2013-12-27

* Thu Nov 14 2013 Bryn M. Reeves <bmr@redhat.com>
- Fix regressions introduced with --build option

* Tue Nov 12 2013 Bryn M. Reeves <bmr@redhat.com>
- Fix typo in yum plug-in add_forbidden_paths
- Add krb5 plug-in and drop collection of krb5.keytab

* Fri Nov  8 2013 Bryn M. Reeves <bmr@redhat.com>
- Add nfs client plug-in
- Fix traceback when sar module force-enabled

* Thu Nov  7 2013 Bryn M. Reeves <bmr@redhat.com>
- Restore --build command line option
- Collect saved vmcore-dmesg.txt files
- Normalize temporary directory paths

* Tue Nov  5 2013 Bryn M. Reeves <bmr@redhat.com>
- Add domainname output to NIS plug-in
- Collect /var/log/squid in squid plug-in
- Collect mountstats and mountinfo in filesys plug-in
- Add PowerPC plug-in from upstream

* Thu Oct 31 2013 Bryn M. Reeves <bmr@redhat.com>
- Remove version checks in gluster plug-in
- Check for usable temporary directory
- Fix --alloptions command line option
- Fix configuration fail regression

* Wed Oct 30 2013 Bryn M. Reeves <bmr@redhat.com>
- Include /etc/yaboot.conf in boot plug-in
- Fix collection of brctl output in networking plug-in
- Verify limited set of RPM packages by default
- Do not strip newlines from command output
- Limit default sar data collection

* Thu Oct 3 2013 Bryn M. Reeves <bmr@redhat.com>
- Do not attempt to read RPC pseudo files in networking plug-in
- Restrict wbinfo collection to the current domain
- Add obfuscation of luci secrets to cluster plug-in
- Add XFS plug-in
- Fix policy class handling of --tmp-dir
- Do not set batch mode if stdin is not a TTY
- Attempt to continue when reading bad input in interactive mode

* Wed Aug 14 2013 Bryn M. Reeves <bmr@redhat.com>
- Add crm_report support to cluster plug-in
- Fix rhel_version() usage in cluster and s390 plug-ins
- Strip trailing newline from command output

* Mon Jun 10 2013 Bryn M. Reeves <bmr@redhat.com>
- Silence 'could not run' messages at default verbosity
- New upstream release

* Thu May 23 2013 Bryn M. Reeves <bmr@redhat.com>
- Always invoke tar with '-f-' option

* Mon Jan 21 2013 Bryn M. Reeves <bmr@redhat.com>
- Fix interactive mode regression when --ticket unspecified

* Fri Jan 18 2013 Bryn M. Reeves <bmr@redhat.com>
- Fix propagation of --ticket parameter in interactive mode

* Thu Jan 17 2013 Bryn M. Reeves <bmr@redhat.com>
- Revert OpenStack patch

* Wed Jan  9 2013 Bryn M. Reeves <bmr@redhat.com>
- Report --name and --ticket values as defaults
- Fix device-mapper command execution logging
- Fix data collection and rename PostreSQL module to pgsql

* Fri Oct 19 2012 Bryn M. Reeves <bmr@redhat.com>
- Add support for content delivery hosts to RHUI module

* Thu Oct 18 2012 Bryn M. Reeves <bmr@redhat.com>
- Add Red Hat Update Infrastructure module
- Collect /proc/iomem in hardware module
- Collect subscription-manager output in general module
- Collect rhsm log files in general module
- Fix exception in gluster module on non-gluster systems
- Fix exception in psql module when dbname is not given

* Wed Oct 17 2012 Bryn M. Reeves <bmr@redhat.com>
- Collect /proc/pagetypeinfo in memory module
- Strip trailing newline from command output
- Add sanlock module
- Do not collect archived accounting files in psacct module
- Call spacewalk-debug from rhn module to collect satellite data

* Mon Oct 15 2012 Bryn M. Reeves <bmr@redhat.com>
- Avoid calling volume status when collecting gluster statedumps
- Use a default report name if --name is empty
- Quote tilde characters passed to shell in RPM module
- Collect KDC and named configuration in ipa module
- Sanitize hostname characters before using as report path
- Collect /etc/multipath in device-mapper module
- New plug-in for PostgreSQL
- Add OpenStack module
- Avoid deprecated sysctls in /proc/sys/net
- Fix error logging when calling external programs
- Use ip instead of ifconfig to generate network interface lists

* Wed May 23 2012 Bryn M. Reeves <bmr@redhat.com>
- Collect the swift configuration directory in gluster module
- Update IPA module and related plug-ins

* Fri May 18 2012 Bryn M. Reeves <bmr@redhat.com>
- Collect mcelog files in the hardware module

* Wed May 02 2012 Bryn M. Reeves <bmr@redhat.com>
- Add nfs statedump collection to gluster module

* Tue May 01 2012 Bryn M. Reeves <bmr@redhat.com>
- Use wildcard to match possible libvirt log paths

* Mon Apr 23 2012 Bryn M. Reeves <bmr@redhat.com>
- Add forbidden paths for new location of gluster private keys

* Fri Mar  9 2012 Bryn M. Reeves <bmr@redhat.com>
- Fix katello and aeolus command string syntax
- Remove stray hunk from gluster module patch

* Thu Mar  8 2012 Bryn M. Reeves <bmr@redhat.com>
- Correct aeolus debug invocation in CloudForms module
- Update gluster module for gluster-3.3
- Add additional command output to gluster module
- Add support for collecting gluster configuration and logs

* Wed Mar  7 2012 Bryn M. Reeves <bmr@redhat.com>
- Collect additional diagnostic information for realtime systems
- Improve sanitization of RHN user and case number in report name
- Fix verbose output and debug logging
- Add basic support for CloudForms data collection
- Add support for Subscription Asset Manager diagnostics

* Tue Mar  6 2012 Bryn M. Reeves <bmr@redhat.com>
- Collect fence_virt.conf in cluster module
- Fix collection of /proc/net directory tree
- Gather output of cpufreq-info when present
- Fix brctl showstp output when bridges contain multiple interfaces
- Add /etc/modprobe.d to kernel module
- Ensure relative symlink targets are correctly handled when copying
- Fix satellite and proxy package detection in rhn plugin
- Collect stderr output from external commands
- Collect /proc/cgroups in the cgroups module
  Resolve: bz784874
- Collect /proc/irq in the kernel module
- Fix installed-rpms formatting for long package names
- Add symbolic links for truncated log files
- Collect non-standard syslog and rsyslog log files
- Use correct paths for tomcat6 in RHN module
- Obscure root password if present in anacond-ks.cfg
- Do not accept embedded forward slashes in RHN usernames
- Add new sunrpc module to collect rpcinfo for gluster systems

* Tue Nov  1 2011 Bryn M. Reeves <bmr@redhat.com>
- Do not collect subscription manager keys in general plugin

* Fri Sep 23 2011 Bryn M. Reeves <bmr@redhat.com>
- Fix execution of RHN hardware.py from hardware plugin
- Fix hardware plugin to support new lsusb path

* Fri Sep 09 2011 Bryn M. Reeves <bmr@redhat.com>
- Fix brctl collection when a bridge contains no interfaces
- Fix up2dateclient path in hardware plugin

* Mon Aug 15 2011 Bryn M. Reeves <bmr@redhat.com>
- Collect brctl show and showstp output
- Collect nslcd.conf in ldap plugin

* Sun Aug 14 2011 Bryn M. Reeves <bmr@redhat.com>
- Truncate files that exceed specified size limit
- Add support for collecting Red Hat Subscrition Manager configuration
- Collect /etc/init on systems using upstart
- Don't strip whitespace from output of external programs
- Collect ipv6 neighbour table in network module
- Collect basic cgroups configuration data

* Sat Aug 13 2011 Bryn M. Reeves <bmr@redhat.com>
- Fix collection of data from LVM2 reporting tools in devicemapper plugin
- Add /proc/vmmemctl collection to vmware plugin

* Fri Aug 12 2011 Bryn M. Reeves <bmr@redhat.com>
- Collect yum repository list by default
- Add basic Infiniband plugin
- Add plugin for scsi-target-utils iSCSI target
- Fix autofs plugin LC_ALL usage
- Fix collection of lsusb and add collection of -t and -v outputs
- Extend data collection by qpidd plugin
- Add ethtool pause, coalesce and ring (-a, -c, -g) options to network plugin

* Thu Apr 07 2011 Bryn M. Reeves <bmr@redhat.com>
- Use sha256 for report digest when operating in FIPS mode

* Tue Apr 05 2011 Bryn M. Reeves <bmr@redhat.com>
- Fix parted and dumpe2fs output on s390

* Fri Feb 25 2011 Bryn M. Reeves <bmr@redhat.com>
- Fix collection of chkconfig output in startup.py
- Collect /etc/dhcp in dhcp.py plugin
- Collect dmsetup ls --tree output in devicemapper.py
- Collect lsblk output in filesys.py

* Thu Feb 24 2011 Bryn M. Reeves <bmr@redhat.com>
- Fix collection of logs and config files in sssd.py
- Add support for collecting entitlement certificates in rhn.py

* Thu Feb 03 2011 Bryn M. Reeves <bmr@redhat.com>
- Fix cluster plugin dlm lockdump for el6
- Add sssd plugin to collect configuration and logs
- Collect /etc/anacrontab in system plugin
- Correct handling of redhat-release for el6

* Thu Jul 29 2010 Adam Stokes <ajs at redhat dot com>

* Thu Jun 10 2010 Adam Stokes <ajs at redhat dot com>

* Wed Apr 28 2010 Adam Stokes <ajs at redhat dot com>

* Mon Apr 12 2010 Adam Stokes <ajs at redhat dot com>

* Tue Mar 30 2010 Adam Stokes <ajs at redhat dot com>
- fix setup.py to autocompile translations and man pages
- rebase 1.9

* Fri Mar 19 2010 Adam Stokes <ajs at redhat dot com>
- updated translations

* Thu Mar 04 2010 Adam Stokes <ajs at redhat dot com>
- version bump 1.9
- replaced compression utility with xz
- strip threading/multiprocessing
- simplified progress indicator
- pylint update
- put global vars in class container
- unittests
- simple profiling
- make use of xgettext as pygettext is deprecated

* Mon Jan 18 2010 Adam Stokes <ajs at redhat dot com>
- more sanitizing options for log files
- rhbz fixes from RHEL version merged into trunk
- progressbar update

